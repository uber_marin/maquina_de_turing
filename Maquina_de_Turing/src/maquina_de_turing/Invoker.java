package maquina_de_turing;


public class Invoker {
  
    
    private Command hacerTransicionCommand;
    private Command mostrarCadenaCommand;
    public Invoker(Command hacerTransicion, Command mostrarCadena) {
        
        this.hacerTransicionCommand = hacerTransicion;
        this.mostrarCadenaCommand= mostrarCadena;
    }
    
     public void hacerTransicion() {
         hacerTransicionCommand.execute();
    }
     
     public void mostrarCadena(){
         mostrarCadenaCommand.execute();
     }
}
