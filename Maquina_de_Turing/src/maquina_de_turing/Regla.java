
package maquina_de_turing;

public class Regla {
    
    private String estado_actual,estado_siguiente,direccion;
    private Character valor_actual,valor_siguiente;

    
    public Regla(String estado_actual,Character valor_actual, String estado_siguiente, Character valor_siguiente, String direccion) {
        this.estado_actual = estado_actual;
        this.estado_siguiente = estado_siguiente;
        this.direccion = direccion;
        this.valor_actual = valor_actual;
        this.valor_siguiente = valor_siguiente;
    }
    
    
    public void setEstado_actual(String estado_actual) {
        this.estado_actual = estado_actual;
    }

    public void setEstado_siguiente(String estado_siguiente) {
        this.estado_siguiente = estado_siguiente;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setValor_actual(Character valor_actual) {
        this.valor_actual = valor_actual;
    }

    public void setValor_siguiente(Character valor_siguiente) {
        this.valor_siguiente = valor_siguiente;
    }
    
    public String getEstado_actual() {
        return estado_actual;
    }

    public String getEstado_siguiente() {
        return estado_siguiente;
    }

    public String getDireccion() {
        return direccion;
    }

    public Character getValor_actual() {
        return valor_actual;
    }

    public Character getValor_siguiente() {
        return valor_siguiente;
    }
    
    
    
}
