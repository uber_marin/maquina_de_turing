package maquina_de_turing;

public interface Command {
      void execute();
}
