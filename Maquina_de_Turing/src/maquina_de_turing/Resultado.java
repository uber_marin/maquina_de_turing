
package maquina_de_turing;


public class Resultado implements Command{
          private Receiver rec;

    public Resultado(Receiver rec) {
        this.rec = rec;
    }
          
   @Override
    public void execute() {
        rec.mostrarCadena(rec.obtenerConfiguracion());
    }
          
}
