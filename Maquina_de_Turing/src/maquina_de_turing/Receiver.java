package maquina_de_turing;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Receiver {
    Configuracion cn=null;
    
    public Configuracion ingresarConfiguracion(){
        StringBuffer palabra= new StringBuffer("abab");
        
        cn = new Configuracion("q1",palabra,0);
        return cn;
    }
    
    public Configuracion obtenerConfiguracion(){
        return cn;
    }
    
    public Regla ingresarRegla (Configuracion conf){
        System.out.println("Ingrese una regla: ");
        Scanner sc = new Scanner(System.in);
        
        String s = sc.nextLine().replaceAll(" ", "");
        Boolean cumplePatron = Pattern.matches("\\([q|Q][0-9]{1}\\,[a-zA-Z]{1}\\)\\=\\([q|Q][0-9]{1}\\,[a-zA-Z]{1}\\,[a-zA-Z]{1}\\)", s);
            if(cumplePatron){
            
          Regla  r = new Regla(Character.toString(s.charAt(1)).toLowerCase()+Character.toString(s.charAt(2)),
                   s.charAt(4), Character.toString(s.charAt(8)).toLowerCase()+Character.toString(s.charAt(9)),
                   s.charAt(11),Character.toString(s.charAt(13)).toUpperCase());
           if(r.getValor_actual().equals('B')){
                r.setValor_actual(' ');
            }
            if(r.getValor_siguiente().equals('B')){
                r.setValor_siguiente(' ');
            }
         
        return r;
        }else{
                System.err.println("La regla esta mal escrita");  
                return null;
        }
    }
    
    public void mostrarCadena(Configuracion conf){
        System.out.println();
         for(int k=0;k<conf.getCadena().length();k++){
                if(k==conf.getI()){
                System.out.print("["+conf.getCadena().charAt(k)+"]"+" ");
                }else{
                System.out.print(conf.getCadena().charAt(k)+" ");
                }
                }
            System.out.println("\nEstado: "+conf.getEstado()+"\n");
            
    }
            
            
    public void hacerTransicion(Regla r,Configuracion conf){
        
      
          if(r!=null){
            
            if(r.getEstado_actual().equals(conf.getEstado()) && r.getValor_actual().equals(conf.getCadena().charAt(conf.getI()))){
                conf.setCadena(conf.getI(), r.getValor_siguiente());
                conf.setEstado(r.getEstado_siguiente());
                if(r.getDireccion().equals("R")){
                conf.setI(conf.getI()+1);
                
                if(conf.getI()+1>conf.getCadena().length()){
                conf.setSpaceFinal();
                }
                
                if(conf.getI()>0 && conf.getCadena().charAt(0)==' '){
                conf.RemoveFirst();
                conf.setI(conf.getI()-1);
                }
                
                }else{
                if(r.getDireccion().equals("L")){
                
               if(conf.getI()< conf.getCadena().length()-1 && conf.getCadena().charAt(conf.getCadena().length()-1)==' '){
                   conf.RemoveFinal();
               }
                if(conf.getI()>0){
                conf.setI(conf.getI()-1);
                }else{
                conf.setSpaceFirst();
                }
              }
            }
          }
              
      }
   
 }
    
}
