
package maquina_de_turing;

public class TransicionCommand implements Command{

    private Receiver rec;


    public TransicionCommand(Receiver rec) {
        this.rec = rec;
    }

    @Override
    public void execute() {
        rec.hacerTransicion(rec.ingresarRegla(rec.obtenerConfiguracion()),rec.obtenerConfiguracion());
        
    }
    
    
}
