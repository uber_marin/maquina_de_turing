package maquina_de_turing;

public class Maquina_de_Turing {
    
    public static void main(String[] args) {
        
        Receiver rec = new Receiver();
        rec.ingresarConfiguracion();
        Command transicion = new TransicionCommand(rec);
        Command resultado = new Resultado(rec);
        
        Invoker in = new Invoker(transicion,resultado);
        
        do{
          in.mostrarCadena();
          in.hacerTransicion();
        
        }while(true);
    }  
}
