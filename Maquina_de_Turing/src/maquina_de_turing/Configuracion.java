package maquina_de_turing;


public class Configuracion {
    
    private int i;
    private String estado;
    StringBuffer cadena = new StringBuffer();
    
    
    
    public Configuracion(String estado, StringBuffer cadena, int i){
      this.estado=estado;
      this.cadena=cadena;
      this.i=i;  
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String estado){
        this.estado=estado;
        
    }
    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
    public StringBuffer getCadena(){
        return cadena;
    }
    
    public void setCadena(int index,Character car){
        this.cadena.setCharAt(index, car);
        
    }
    
    public void setSpaceFinal(){
        this.cadena.insert(this.cadena.length(),' ');
        
    }
    
    public void setSpaceFirst(){
        this.cadena.insert(0, ' ');

    }
    
    public void RemoveFirst(){
        this.cadena.deleteCharAt(0);
    }
    
    public void RemoveFinal(){
        this.cadena.deleteCharAt(this.cadena.length()-1);
    }

    
    
}
